﻿using UnityEngine;

namespace Assets.WorldScripts
{
    class AirBlock : Block
    {
        public AirBlock(int posX, int posY) : base(posX,posY)
        {
            IsSolid = false;
            CanBuild = true;
            PrefabName = "AirBlock";
        }
    }
}
