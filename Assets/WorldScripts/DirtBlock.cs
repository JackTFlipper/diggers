﻿using UnityEngine;

namespace Assets.WorldScripts
{
    class DirtBlock : Block
    {
        public DirtBlock(int posX,int posY) : base(posX,posY)
        {
            IsSolid = true;
            CanBuild = false;
            PrefabName = "DirtBlock";
        }

        public override void Destroyed()
        {
            //drop item dirtblock
        }
    }
}
