﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.WorldScripts;

public class WorldGenerator : MonoBehaviour {

    public int worldWidth = 10;
    public int worldHeight = 10;

    private int worldNegWidth;
    private int worldPosWidth;
    private int worldPosHeight;
    private int worldNegHeight;

    private Dictionary<Vector2,Block> worldBlocks;

    // Use this for initialization
    void Start ()
    {
        worldBlocks = new Dictionary<Vector2, Block>();
        worldNegWidth = (worldWidth / 2) * -1;
        worldPosWidth = worldWidth / 2;
        worldPosHeight = worldHeight / 5; //  1/5 air
        worldNegHeight = (worldHeight / 5)*-4; //  4/5 underground
        Block block = null;

        for(int x = worldNegWidth; x <worldPosWidth;x++)
        {
            for (int y = worldNegHeight; y < worldPosHeight; y++)
            {
                if (y > 0)
                {
                    block = new AirBlock(x, y);
                }
                else
                {
                    block = new DirtBlock(x, y);
                }
                worldBlocks.Add(new Vector2(x, y), block);
                block.RenderBlock(gameObject);

            }
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}


