﻿using UnityEngine;

namespace Assets.WorldScripts
{
    public abstract class Block
    {
        private Vector2 coordinates;
        private bool isSolid;
        private bool canBuild;

        public Block(int Xpos, int Ypos)
        {
            coordinates = new Vector2(Xpos, Ypos);
        }

        public virtual void Destroyed()
        {

        }

        public virtual bool RenderBlock(GameObject parent)
        {
            try
            {
                BlockObj = (GameObject)GameObject.Instantiate(Resources.Load("Prefabs/"+PrefabName), parent.transform);
                BlockObj.name = PrefabName+" " + Coordinates.x + "," + Coordinates.y;
                BlockObj.transform.localScale = new Vector3(4, 4, 1);
                BlockObj.transform.position = new Vector3(Coordinates.x*4 , Coordinates.y*4, 0);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public Vector2 Coordinates
        {
            get
            {
                return coordinates;
            }
        }

        public bool IsSolid
        {
            get
            {
                return isSolid;
            }
            protected set
            {
                isSolid = value;
            }
        }

        public bool CanBuild
        {
            get
            {
                return canBuild;
            }
            protected set
            {
                canBuild = value;
            }
        }

        protected string PrefabName;
        protected GameObject BlockObj;

    }
}