﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamea : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        float xAxisValue = Input.GetAxis("Horizontal");
        float YAxisValue = Input.GetAxis("Vertical");
        if (Camera.current != null)
        {
            Camera.current.transform.Translate(new Vector3(xAxisValue, YAxisValue, 0));
        }
    }


}
