﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectBlock : MonoBehaviour {

    private GameObject HighLighter;
    private GameObject Selected;
    private bool selected;

    private void Start()
    {
        HighLighter = transform.Find("HighLighter").gameObject;
        Selected = transform.Find("Selected").gameObject;
        selected = false;
    }

    private void OnMouseEnter()
    {
        HighLighter.SetActive(true);
    }

    private void OnMouseExit()
    {
        HighLighter.SetActive(false);
    }

    private void OnMouseUpAsButton()
    {
        selected = !selected;
        Selected.SetActive(selected);
    }
}
